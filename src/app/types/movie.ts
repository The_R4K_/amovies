export interface Movie{
    title: string,
    description: string,
    rating: number,
    image: string,
    id: number
}