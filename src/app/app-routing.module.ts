import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogComponent } from './components/catalog/catalog.component';
import { AboutComponent } from './components/common/about/about.component';
import { ErrorComponent } from './components/common/error/error.component';
import { MovieEditComponent } from './components/movie-edit/movie-edit.component';
import { MovieInfoComponent } from './components/movie-info/movie-info.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'movies' },
  { path: `movies`, component: CatalogComponent },
  { path: `movies/:id`, component: MovieInfoComponent },
  { path: `movies/edit/:id`, component: MovieEditComponent },
  { path: `about`, component: AboutComponent },
  { path: 'error/:code',  component: ErrorComponent},
  { path: '**', redirectTo: 'error/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
