import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MovieServiceService } from 'src/app/services/movie-service.service';
import { Movie } from 'src/app/types/movie';

@Component({
  selector: 'app-movie-edit',
  templateUrl: './movie-edit.component.html',
  styleUrls: ['./movie-edit.component.css']
})
export class MovieEditComponent implements OnInit, OnDestroy {
  movie: Movie;
  sub: Subscription;

  constructor(private route:ActivatedRoute, private router:Router, private movieService: MovieServiceService) { }

  ngOnDestroy(): void {
    //this.sub.unsubscribe();
  }

  ngOnInit(): void {
      this.sub = this.route.params.subscribe(param => {
        this.movieService.getMovie(param['id']).subscribe(
          m => {this.movie = m}
        )
      },
      error => {this.router.navigate(['/error','404'])}
      );
  }

  onSubmit(){
      //console.log(this.movie);

      this.movieService.updateMovie(this.movie).subscribe(
        m => { this.router.navigate(['/movies', this.movie.id]);}//m.id]); }
      );
  }
}
