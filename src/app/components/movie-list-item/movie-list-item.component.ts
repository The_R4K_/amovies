import { Component, OnInit, Input, AfterContentInit, ViewChild, ElementRef, HostListener, AfterViewInit } from '@angular/core';
import { DescriptionState } from 'src/app/types/DescriptionState';
import { Movie } from 'src/app/types/movie';

@Component({
  selector: 'app-movie-list-item',
  templateUrl: './movie-list-item.component.html',
  styleUrls: ['./movie-list-item.component.css']
})
export class MovieListItemComponent implements OnInit, AfterViewInit{
  @Input() movie: Movie;
  @ViewChild('desc') elementDesc: ElementRef;
  descState: DescriptionState = DescriptionState.NONE;
  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void{
    this.updateTextCollapser();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void{
    this.updateTextCollapser();
  }

  updateTextCollapser(): void{
    if(this.elementDesc.nativeElement.offsetHeight > 149)
      this.descState = DescriptionState.COLLAPSED;
    else
      this.descState = DescriptionState.NONE;
  }

  showDesc(): void{
    if(this.descState==DescriptionState.NONE)
      return;

    if(this.descState==DescriptionState.EXPANDED)
      this.descState=DescriptionState.COLLAPSED;
    else
      this.descState=DescriptionState.EXPANDED;
  }

  setDescClasses(){
    return{
      "movieDescription" : this.descState==DescriptionState.NONE,
      "movieDescriptionHide": this.descState==DescriptionState.COLLAPSED,
      "movieDescriptionShow": this.descState==DescriptionState.EXPANDED
    }
  }

  setDescShowClasses(){
    return{
    "movieShowMore": true,
    "hidden": this.descState==DescriptionState.NONE
    }
  }

  setRatingClasses(){
    return {
      "movieRatingContainer" : true,
      "badRatingBg" : this.movie.rating<3,
      "notbadRatingBg" : this.movie.rating>=3 && this.movie.rating<7,
      "coolRatingBg" : this.movie.rating>=7,
    }
  }
}
