import { Component, OnInit } from '@angular/core';
import { faThList, faThLarge } from '@fortawesome/free-solid-svg-icons';
import { MovieServiceService } from 'src/app/services/movie-service.service';
import { Movie } from 'src/app/types/movie';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  model = 1;
  faThList = faThList;
  faThLarge = faThList;

  movies: Movie[];

  constructor(private movieService: MovieServiceService) { }

  ngOnInit(): void {
    this.movieService.getMovies().subscribe(
      m => {this.movies = m;
      console.log(m);}
    );
  }

}
