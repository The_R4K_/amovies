import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MovieServiceService } from 'src/app/services/movie-service.service';
import { Movie } from 'src/app/types/movie';

@Component({
  selector: 'app-movie-info',
  templateUrl: './movie-info.component.html',
  styleUrls: ['./movie-info.component.css']
})
export class MovieInfoComponent implements OnInit, OnDestroy {
  movie: Movie;
  sub: Subscription;
  constructor(private route:ActivatedRoute, private router:Router, private movieService: MovieServiceService) { }

  ngOnDestroy(): void {
    //this.sub.unsubscribe();
  }

  ngOnInit(): void {
      this.route.params.subscribe(param => {
        this.movieService.getMovie(param['id']).subscribe(
          m => {this.movie = m}
        )
      },
      error => {this.router.navigate(['/error','404'])}
      );
  }

  setRatingClasses(){
    return {
      "movieRatingContainer" : true,
      "badRatingBg" : this.movie.rating<3,
      "notbadRatingBg" : this.movie.rating>=3 && this.movie.rating<7,
      "coolRatingBg" : this.movie.rating>=7,
    }
  }
}
