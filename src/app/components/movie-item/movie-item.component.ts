import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/types/movie';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.css']
})
export class MovieItemComponent implements OnInit {
  @Input() movie: Movie;
  constructor() { }

  ngOnInit(): void {
  }

  setRatingClasses(){
    return {
      "movieRatingContainer" : true,
      "badRatingBg" : this.movie.rating<3,
      "notbadRatingBg" : this.movie.rating>=3 && this.movie.rating<7,
      "coolRatingBg" : this.movie.rating>=7,
    }
  }
}
