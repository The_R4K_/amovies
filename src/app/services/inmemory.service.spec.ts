import { TestBed } from '@angular/core/testing';

import { InmemoryService } from './inmemory.service';

describe('InmemoryService', () => {
  let service: InmemoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InmemoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
