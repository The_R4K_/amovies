import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from '../types/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieServiceService {
  uri:string = 'api/movies';

  constructor(private http:HttpClient) { }

  getMovies(): Observable<Movie[]>{
    return this.http.get<Movie[]>(this.uri);
  }

  getMovie(id: number): Observable<Movie>{
    return this.http.get<Movie>(`${this.uri}/${id}`);
  }

  createMovie(movie: Movie){
    return this.http.post(this.uri, movie);
  }

  deleteMovie(id: number){
      return this.http.delete(`${this.uri}/${id}`);
  }

  updateMovie(movie: Partial<Movie>): Observable<Movie>{
      return this.http.put<Movie>(`${this.uri}/${movie.id}`, movie);
  }
}
