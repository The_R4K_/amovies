import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CatalogComponent } from './components/catalog/catalog.component';
import { MovieItemComponent } from './components/movie-item/movie-item.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { ErrorComponent } from './components/common/error/error.component';
import { AboutComponent } from './components/common/about/about.component';
import { MovieInfoComponent } from './components/movie-info/movie-info.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MovieListItemComponent } from './components/movie-list-item/movie-list-item.component';
import { MovieEditComponent } from './components/movie-edit/movie-edit.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InmemoryService } from './services/inmemory.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CatalogComponent,
    MovieItemComponent,
    HeaderComponent,
    ErrorComponent,
    AboutComponent,
    MovieInfoComponent,
    MovieListItemComponent,
    MovieEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InmemoryService)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
